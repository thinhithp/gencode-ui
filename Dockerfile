## Sử dụng Nginx image từ Docker Hub
#FROM nginx:alpine
#
## Copy các tệp tĩnh từ thư mục dự án frontend của bạn vào thư mục Nginx
#COPY .. /usr/share/nginx/html
#
## Cổng mặc định mà Nginx lắng nghe là 80
#EXPOSE 8081

# Sử dụng Caddy image từ Docker Hub
FROM caddy:2-alpine

# Đặt thư mục làm việc
WORKDIR /usr/share/caddy

# Copy các tệp tĩnh từ thư mục dự án vào thư mục Caddy
COPY gencode/ .

# Caddyfile cấu hình mặc định phục vụ tệp tĩnh từ /usr/share/caddy
# Không cần cấu hình thêm nếu bạn chỉ cần phục vụ tệp tĩnh

# EXPOSE không cần thiết vì Caddy mặc định lắng nghe trên cổng 80 và 443
