document.getElementById('selectAll').addEventListener('click', function (event) {
    var checkboxes = document.querySelectorAll('input[name="rowSelect"]');
    for (var checkbox of checkboxes) {
        checkbox.checked = event.target.checked;
    }
});
document.getElementById('genButton').addEventListener('click', function () {
    var columnInput = document.querySelector('input[aria-label="tenCot"]');
    var urlInput = document.querySelector('input[aria-label="url"]');
    var userNameInput = document.querySelector('input[aria-label="userName"]');
    var passwordInput = document.getElementById('passwordInput');

    // Check if all inputs are valid
    if (columnInput.checkValidity() && urlInput.checkValidity() &&
        userNameInput.checkValidity() && passwordInput.checkValidity()) {

        // All inputs are valid, proceed with the POST request
        var requestBody = {
            table: columnInput.value.split(','),
            url: urlInput.value,
            useName: userNameInput.value, // Use "userName" here
            password: passwordInput.value
        };

        axios.post('http://' + window.location.hostname + ':8080/v1/gen-code/gen-code', requestBody)
            .then(function (response) {
                var filePath = response.data;
                console.log('File Path:', filePath);
                readFile(filePath);
            })
            .catch(function (error) {
                console.error('Error:', error);
            });
    } else {
        // One or more inputs are invalid, display an error message or handle accordingly
        alert('Please fill in all required fields.');
    }
});

// document.getElementById('genButton').addEventListener('click', function () {
//     // Collect input values
//     var columnNames = document.querySelector('input[aria-label="tenCot"]').value;
//     var url = document.querySelector('input[aria-label="url"]').value;
//     var userName = document.querySelector('input[aria-label="userName"]').value;
//     var password = document.getElementById('passwordInput').value;
//
//     // Prepare requestBody
//     var requestBody = {
//         table: columnNames.split(','),
//         url: url,
//         useName: userName, // Use "userName" here
//         password: password
//     };
//
//     // Make POST request to gen-code/test
//     axios.post('http://localhost:8082/v1/gen-code/test', requestBody)
//         .then(function (response) {
//             // Handle success
//             var filePath = response.data; // Assuming response.data contains the file path
//             console.log('File Path:', filePath);
//             readFile(filePath); // Call the read-file API
//         })
//         .catch(function (error) {
//             // Handle error
//             console.error('Error:', error);
//         });
// });
document.getElementById('downloadButton').addEventListener('click', function () {
    var selectedFiles = [];
    var checkboxes = document.querySelectorAll('input[name="rowSelect"]:checked');
    checkboxes.forEach(function (checkbox) {
        selectedFiles.push(checkbox.value);
    });

    if (selectedFiles.length > 0) {
        downloadFiles(selectedFiles);
    } else {
        console.log('Không có file nào được chọn.');
    }
});

function readFile() {
    // Make GET request to read-file without any parameters
    axios.get('http://' + window.location.hostname + ':8080/v1/gen-code/read-file')
        .then(function (response) {
            // Handle success
            console.log('File Data:', response.data);
            populateTable(response.data); // Populate the table with file data
        })
        .catch(function (error) {
            // Handle error
            console.error('Error:', error);
        });
}


// function populateTable(data) {
//     var tableBody = document.querySelector('.table tbody');
//     tableBody.innerHTML = ''; // Clear existing rows
//
//     data.forEach(function(file, index) {
//         var row = `<tr>
//         <td><input type="checkbox" name="rowSelect" value="${file}"></td>
//         <td>${index + 1}</td>
//         <td>${file}</td> <!-- Assuming each item is a file name -->
//     </tr>`;
//         tableBody.innerHTML += row;
//     });
// }
function downloadSingleFile(fileName) {
    // Tạo một danh sách chỉ chứa một file
    var fileList = [fileName];
    // Gọi hàm downloadFiles với danh sách này
    downloadFiles(fileList);
}

function downloadFiles(fileList) {
    // Make POST request to gen-code/download
    var fileName = document.getElementById('fileNameInput').value.concat(".zip") || 'download.zip';

    axios.post('http://' + window.location.hostname + ':8080/v1/gen-code/download', fileList, {responseType: 'blob'})
        .then(function (response) {
            // Handle success
            console.log('Download successful');
            saveAsFile(response.data, fileName); // Example file name: download.zip
        })
        .catch(function (error) {
            // Handle error
            console.error('Error:', error);
        });
}


function saveAsFile(blob, fileName) {
    var url = window.URL.createObjectURL(blob);
    var a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
}

function populateTable(data) {
    var tableBody = document.querySelector('.table tbody');
    tableBody.innerHTML = ''; // Clear existing rows

    data.forEach(function (file, index) {
        var row = `<tr>
            <td><input type="checkbox" name="rowSelect" value="${file}"></td>
            <td>${index + 1}</td>
            <td>${file}</td>
            <td><button class="downloadButton" data-file="${file}">Download</button></td>
        </tr>`;
        tableBody.innerHTML += row;
    });

    // Add event listeners to the newly created download buttons
    document.querySelectorAll('.downloadButton').forEach(function (button) {
        button.addEventListener('click', function (event) {
            var fileName = event.target.getAttribute('data-file');
            downloadSingleFile(fileName);
        });
    });
}
